// Mobile Menu
$('.btn-mobile').click(function() {
  $(this).toggleClass('is-active');
  $('.m-mainmenu').slideToggle(500);
});

// fix menu on resize windows when its close!
$(window).resize(function() {
  if ($(window).width() > 768) {
    $('.m-mainmenu').show();
  } else {
    $('.m-mainmenu').hide();
  }
});

// BlockQuote Slider
$('#quote-slider').bxSlider({
  pagerCustom: '#bx-pager',
  controls: false,
});
